Course Cheatsheet
-----------------

# Bash
Helpful ~/.bashrc
-----------------
```
# append to the history file, don't overwrite it
shopt -s histappend

# Entries beginning with space aren't added into history, and duplicate
# entries will be erased (leaving the most recent entry).
# Lots o' history.
export HISTSIZE=10000
export HISTFILESIZE=10000

# leave some commands out of history log
export HISTIGNORE="&:??:[ ]*:clear:exit:logout"

export EDITOR='vim'

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
```

# Git
## Resources
[Git Cheatsheet](https://www.git-tower.com/blog/git-cheat-sheet/)

## Commands
```
# View current state of working area and staging
git status

# Make a directory a git repo
git init

# Make and checkout branch
git checkout -b new-branch-name

# Delete a branch that has been merged
git branch -d old-branch-name

# Stage all files in directory to git
git add .

# Commit all staged files
git commit -m "commit message"

# Update the master branch in the remote "origin" repository
git push origin master

# Checkout master and pull latest changes
git checkout master
git pull origin master
```

# Python
## Script Skeleton
```
#! /usr/bin/env python

def do_stuff():
    print("Hello world!")

if __name__ == "__main__":
    do_stuff()
```

Run the script with `python3 script.py`
## Unittesting

[Unittest library](https://docs.python.org/3/library/unittest.html)


```
import unittest
class TestMyCode(unittest.TestCase):
    def test_is_true(self):
        self.assertTrue(True)

```
