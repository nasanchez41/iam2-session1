inputvar=$#
all_args=$*

 echo "inputs arg# is $inputvar "
if [[ $inputvar -eq  0  ]]; then 
	echo "Argument expected "
	exit 0
fi


echo "all files are $* "

for f in $* ;  do 
	if [[ ! -f $f ]] ; then  
		echo "filename $f does not  exists"
		continue
	fi	
	## echo $f
	if [[ ! -s $f ]] ; then 
		echo "file $f is zero-byte, remove"
		echo "rm $f "
		continue
	fi
	echo "inspected " >> $f 
done
