import unittest
import json
import unittest.mock
from unittest import mock
import pylog


class MockResponse:

    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code

    def json(self):
        return json.loads(self.text)

    def __iter__(self):
        return self

    def __next__(self):
        return self


def mock_requests_get_success(*args, **kwargs):
    text = """
    [{
        "id": 10,
        "date": "2020-03-04T14:34:20",
        "date_gmt": "2020-03-04T14:34:20",
        "guid": {
            "rendered": "http://ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088/2020/03/04/top-10-vacation-sites/"
        },
        "modified": "2020-03-04T14:34:20",
        "modified_gmt": "2020-03-04T14:34:20",
        "slug": "top-10-vacation-sites",
        "status": "publish",
        "type": "post",
        "link": "http://ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088/2020/03/04/top-10-vacation-sites/",
        "title": {
            "rendered": "Top 10 vacation sites"
        },
        "content": {
            "rendered": "<p>&#8220;This is a list list of top vacation sites. 1. kayak, 2. priceline&#8221;</p>",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>&#8220;This is a list list of top vacation sites. 1. kayak, 2. priceline&#8221;</p>",
            "protected": false
        }
    }]
    """  # noqa: E501
    response = MockResponse(text, 200)
    return response


def mock_requests_get_empty_blogs(*args, **kwargs):
    text = """[]"""
    response = MockResponse(text, 200)
    return response


def mock_requests_get_error(*args, **kwargs):
    response = MockResponse("Received bad response from wordpress", 500)
    return response


def mock_requests_upload_error(*args, **kwargs):
    response = MockResponse("Upload failed", 500)
    return response


def mock_requests_upload_post(*args, **kwargs):
    text = """
    {
    "id": 9,
    "date": "2020-03-04T14:31:36",
    "date_gmt": "2020-03-04T14:31:36",
    "guid": {
        "rendered": "http://ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088/2020/03/04/top-10-movies/",
        "raw": "http://ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088/2020/03/04/top-10-movies/"
    },
    "modified": "2020-03-04T14:31:36",
    "modified_gmt": "2020-03-04T14:31:36",
    "password": "",
    "slug": "top-10-movies",
    "status": "publish",
    "type": "post",
    "link": "http://ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088/2020/03/04/top-10-movies/"
    }
    """  # noqa: E501
    response = MockResponse(text, 201)
    return response


class TestLatestPost(unittest.TestCase):

    @unittest.mock.patch('requests.request', mock_requests_get_success)
    def test_get_latest_post(self):
        title, content = pylog.get_latest_post()
        self.assertEqual("Top 10 vacation sites", title)
        self.assertEqual("<p>&#8220;This is a list list of top vacation sites. 1. kayak, 2. priceline&#8221;</p>", content)  # noqa: E501

    @unittest.mock.patch('requests.request', mock_requests_get_error)
    def test_get_latest_post_error(self):
        with self.assertRaises(Exception):
            pylog.get_latest_post()

    @unittest.mock.patch('requests.request', mock_requests_get_empty_blogs)
    def test_get_latest_post_none(self):
        no_post = pylog.get_latest_post()
        self.assertEqual("No Blog Posts", no_post)


class TestUploadPost(unittest.TestCase):

    @unittest.mock.patch('requests.request', mock_requests_upload_post)
    def test_upload_post(self):
        data_id, data_status, data_type = pylog.upload_post('blog_title', 'blog_content')  # noqa: E501
        self.assertEqual(9, data_id)
        self.assertEqual("publish", data_status)
        self.assertEqual("post", data_type)

    def test_file_parse(self):
        FILENAME = "./pylog/fake_post"
        blog_title, blog_content = pylog.parse_file(FILENAME)
        self.assertEqual(blog_title, "This is a test of the file parsing function")  # noqa: E501
        self.assertEqual("".join(blog_content), "This is a test for uploading a blog post from a file. This is only a test. ")  # noqa: E501

    @unittest.mock.patch('requests.request', mock_requests_upload_error)
    def test_upload_post_failed(self):
        with self.assertRaises(Exception):
            blog_title = "Test Title"
            blog_content = 'Test Content'
            pylog.upload_post(blog_title, blog_content)


class TestCreatePostTitle(unittest.TestCase):
    def test_create_post_title(self):
        with mock.patch('builtins.input', return_value="title"):
            self.assertEqual(pylog.create_post_title(), 'title')
