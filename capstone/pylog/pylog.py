import requests
import argparse
import os

# ####Parameters#####
"""WORDPRESS_SITE = "ec2-3-132-215-238.us-east-2.compute.amazonaws.com:8088"""
FILENAME = ''
WP_USERNAME = os.getenv('WORDPRESS_USERNAME')
WP_PASSWORD = os.getenv('WORDPRESS_PASSWORD')
WP_URL = os.getenv('WORDPRESS_URL')
###################


def getArgs():  # pragma: no cover
    parser = argparse.ArgumentParser()
    parser.add_argument("--read",
                        help="Read the latest blog post",
                        action="store_true",
                        required=False)
    parser.add_argument("--upload",
                        help="Upload a new post from file",
                        action="store_true",
                        required=False)
    parser.add_argument("--f",
                        help="Specify file for to upload as a new blog",
                        required=False)
    parser.add_argument("--i",
                        help="Specify blog title and content",
                        action="store_true",
                        required=False)
    return parser.parse_args()


def get_latest_post():
    api_endpoint = "/wp-json/wp/v2/posts"
    url = "http://{}/{}".format(WP_URL, api_endpoint)
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers,
                                auth=(WP_USERNAME, WP_PASSWORD), data=payload)
    if response.status_code != 200:
        # data = response.json()
        raise Exception(
            "Received bad response from wordpress.")  # %s" % data)
    data = response.json()
    if data == []:
        no_post = "No Blog Posts"
        print(no_post)
        return no_post
    data = response.json()[0]
    title = data['title']['rendered']
    content = data['content']['rendered']
    print(title, content)
    return title, content


def parse_file(filename):
    BLOG_FILE = [line.rstrip('\n') for line in open(filename)]
    blog_title = BLOG_FILE[0]
    blog_content = BLOG_FILE[2:]
    return blog_title, blog_content


def upload_post(blog_title, blog_content):
    api_endpoint = "/wp-json/wp/v2/posts"
    string_kwargs = {
        "base_url": WP_URL,
        "base_path": api_endpoint,
        "title": "title=" + str(blog_title),
        "author": "author=2",
        "comment": "comment_status=open",
        "p_format": "format=standard",
        "content": "content=" + "\n".join(blog_content),
        "status": "status=publish"
    }
    format_string = "http://{base_url}/{base_path}?{title}&{author}&{comment}&{p_format}&{content}&{status}"  # noqa: E501
    url = format_string.format(**string_kwargs)
    payload = {}
    headers = {
    }

    response = requests.request("POST", url, headers=headers,
                                auth=(WP_USERNAME, WP_PASSWORD), data=payload)
    if response.status_code == 201:
        data = response.json()
        data_id = data['id']
        data_status = data['status']
        data_type = data['type']
        print("Blog Number =", data_id,
              "Status =", data_status,
              "Action =", data_type)
        return data_id, data_status, data_type
    else:
        # data = response.json()
        raise Exception("Upload failed")  # , data)


def create_post_title():
    blog_title = input("Enter in blog title: ")

    return blog_title


def create_post_content():  # pragma: no cover
    print("Enter/Paste your content. Ctrl-D ( linux ) or Ctrl-Z ( windows ) to save it.")  # noqa: E501
    blog_content = []
    while True:
        try:
            line = input()
        except EOFError:
            break
        blog_content.append(line)
    return blog_content


def main():  # pragma: no cover
    args = getArgs()
    FILENAME = args.f
    if args.read:
        get_latest_post()
    if args.upload:
        blog_title, blog_content = parse_file(FILENAME)
        upload_post(blog_title, blog_content)
    if args.i:
        blog_title, blog_content = create_post_title(), create_post_content()
        upload_post(blog_title, blog_content)


if __name__ == "__main__":
    main()  # pragma: no cover
