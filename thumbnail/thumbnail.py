from PIL import Image
from PIL import _imaging
import os  
import boto3
from botocore.exceptions import ClientError
import logging

max_width = 256
max_height = 256
image_format = "jpg"
output_dir = "/tmp/thumbnails"
#output_bucket = "ameade-thumbnail-images"
output_bucket = "asanchez-thumbnail-images"

def handler(event, context):  
  bucket = event['bucket']
  key = event['object']

    
  try:
    #tmp = "/tmp/" + key
    tmp = key
    download(bucket, key, tmp)


    # resize
    resized = resized_path(key)
    resize(tmp, resized)

    # upload resized image
    upload(resized, output_bucket, get_filename(key) + ".jpg")

    return  

  except Exception as e:  
      print(e)  
      
def resized_path(key):
  name = get_filename(os.path.basename(key))
  return '/tmp/{}.{}'.format(name, image_format)

def resize(src, dest):
  # pil image processing
  img = Image.open(src, 'r')  
  img.thumbnail((max_width, max_height), Image.ANTIALIAS)  
  print('format: ' + img.format)
  if img.format == 'PNG' and image_format == 'jpg':
    img = img.convert('RGB')
  print('image resized')
  return img.save(dest)

def download(source_bucket, source_object_key, tmp):
  """ Download the specified object to the tmp location """
  # TODO
  s3 = boto3.client('s3')
  #with open(tmp, 'wb') as f:
  #  s3.download_fileobj('source_bucket','source_object_key', f) 
  print(source_bucket, "+ " , source_object_key , "+ ", tmp )
  s3.download_file(source_bucket, source_object_key, tmp) 
  return 

def upload(src, dest_bucket, dest_object):
  """ Upload the specified src file to the specific object"""
  # TODO
  s3_client = boto3.client('s3')
  try:
    response = s3_client.upload_file(src, dest_bucket, dest_object)
  except ClientError as e:
    logging.error(e)
    return False
  return True


def get_filename(key):
  filename = os.path.basename(key)  
  name, ext = os.path.splitext(filename)
  return name


if __name__ == "__main__":
    event = {
      "bucket": "asanchez-original-images",
      "object": "flower.png",
    }
    handler(event, None)
