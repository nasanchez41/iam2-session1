import math
import random
import sys

# Grab size of deck from command line
size_of_deck = int(sys.argv[1])
original_deck = list(range(1, size_of_deck + 1))
deck = original_deck[:]

total_count = 0
iterations = 10_000

for i in range(1, iterations + 1):
    if i % 100 == 0:
        print(i, end=' ', flush=True)
    count = 1
    while True:
        index = random.randint(0, size_of_deck - 1)
        deck[0], deck[index] = deck[index], deck[0]
        if deck == original_deck:
            break
        count += 1
    total_count += count

print()
print(total_count / iterations, math.factorial(size_of_deck))
        
