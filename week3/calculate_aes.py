#!/usr/bin/env python

import sys 


def calculate(num1, num2, op):
    if op not in '+-*/':
        print('Bad operator:', op)
        return None

    if op == '+':
        return num1 + num2
    if op == '-':
        return num1 - num2
    if op == '*':
        return num1 * num2
    if op == '/':
        try:
            if num2 ==  0 :
                raise ZeroDivisionError('zero is not my hero')
        except ZeroDivisionError:
            print('exception caught')
            return
        return num1 / num2

num1 = int(sys.argv[1])
num2 = int(sys.argv[2])
op   = sys.argv[3]


print (num1, num2, op )
print(calculate(3,0 , '+'))
print(calculate(3,0 , '-'))
print(calculate(0,3 , '-'))
print(calculate(3,0 , '*'))
print(calculate(0,3 , '*'))
print(calculate(0,3 , '/'))
print(calculate(3,0 , '/'))
