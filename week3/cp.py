class ControlPanel:
    MIN_TEMP = 0.0
    MAX_TEMP = 100.0

    def __init__(self):
        self.__temp = 22 # room temp in Celsius

    @property
    def temperature(self):
        return self.__temp

    @temperature.setter
    def temperature(self, val):
        if self.MIN_TEMP <= val <= self.MAX_TEMP:
            self.__temp = val
        else:
            raise ValueError('Temp outside range ' + \
		str(self.MIN_TEMP) + '...' + str(self.MAX_TEMP))



