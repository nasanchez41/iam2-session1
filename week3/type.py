def func(x: int) -> float:
    return 2.0 * x

a: int = 1
b: float = 1.1

print(func(a))
print(func(b))
