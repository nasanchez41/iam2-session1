import sys
import re

in_source = False

for line in sys.stdin:
    if in_source:
        if re.search(r']$', line):
            in_source = False
        else:
            print(line, end='')
    elif re.search(r'"source": \[', line):
        in_source = True
